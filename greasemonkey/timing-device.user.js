// ==UserScript==
// @name           時限装置
// @namespace      http://nobby451.bitbucket.org/
// @description    一定間隔で処理を実行する
// @include        https://www.google.co.jp/*
// ==/UserScript==

setInterval(function() {
  if (!(new Date().getSeconds() % 10)) {
    document.getElementById('gbqfq').value = ['Chromium', 'Firefox', 'Epiphany', 'rekonq'][Math.floor(Math.random() * 4)];
    document.getElementById('gbqf').submit();
  }
}, 1000);
