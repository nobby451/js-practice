window.onload = function() {// ページの読み込み完了時に匿名関数を実行
	if (!((navigator.userAgent.indexOf('iPhone') > 0 
			&& navigator.userAgent.indexOf('iPad') == -1)
			|| navigator.userAgent.indexOf('iPod') > 0
			|| navigator.userAgent.indexOf('Android') > 0)) {// User Agentでデバイスを判定
		var anchor1 = document.getElementById('link-enable-1');// 指定したidで識別されるa要素を取得
		anchor1.href = null;// a要素のhref属性を未設定に

		var anchor2 = document.getElementById('link-enable-2');
		var image = anchor2.firstElementChild;// a要素の最初の子要素を取得
		var parent = anchor2.parentNode;// a要素の親要素を取得
		parent.replaceChild(image, anchor2);// a要素をimg要素で置き換える
		/*
		 * 変数をインライン化してこのようにも書ける
		 * anchor2.parentNode.replaceChild(anchor2.firstElementChild, anchor2);
		 */

		$('#link-enable-3 > img').unwrap();// jQueryのセレクターでマッチした要素の親要素を除去
	}
};
